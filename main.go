package main

import (
	"fmt"

	"netacom.vn/netachat/config"
)

func main() {
	fmt.Printf("Server started :%+v \n", config.AppConfig)
}
