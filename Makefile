BINARY=engine

gen-otp:
	protoc OTP/delivery/grpc/pb/otp.proto --go_out=paths=source_relative,plugins=grpc:.

run-server:
	go run sms-grpc/server.go

run-client:
	go run sms-grpc/client/client.go 

test:
	go test -v -cover -covermode=atomic ./...

engine:
	go build -o ${BINARY} app/*.go

engine-grpc:
	go build -o ${BINARY} sms-grpc/*.go

unittest:
	go test -short  ./...

clean:
	if [ -f ${BINARY} ] ; then rm ${BINARY} ; fi

docker-grpc:
	CGO_ENABLED=0 docker build -f docker/Dockerfile -t netalo-sms-service .

# docker:
# 	CGO_ENABLED=0 docker build -f docker/http/Dockerfile -t netalo .

run:
	docker-compose up --build -d

stop:
	docker-compose down

lint-prepare:
	@echo "Installing golangci-lint"
	curl -sfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh| sh -s latest

lint:
	./bin/golangci-lint run ./...

.PHONY: clean install unittest build docker docker-grpc run stop vendor lint-prepare lint