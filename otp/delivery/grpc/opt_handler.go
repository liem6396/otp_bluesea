package otpHandler

import (
	"context"
	"encoding/base64"
	"fmt"

	message "netacom.vn/netachat/domain"
	sms "netacom.vn/netachat/otp/delivery/grpc/pb"
	"netacom.vn/netachat/otp/usecase"
)

type OTPServer struct {
	OTPUsecase message.OTPUseCase
}

func (otps *OTPServer) SendOTPSMS(ctx context.Context, req *sms.OTPMTRequest) (*sms.OTPMTResponse, error) {
	user := message.SendMessageRequest{
		Phone: req.Phone,
		Text:  base64.StdEncoding.EncodeToString([]byte(req.OTP)),
	}

	httpReq, err := usecase.GenerateSOAPRequestMTOTP(&user)
	if err != nil {
		fmt.Println("Some problem occurred in request generation")
	}

	response, err := usecase.SoapCallMTOTP(httpReq)
	if err != nil {
		fmt.Println("Problem occurred in making a SOAP call")
	}
	result := fmt.Sprintf("SOAP response: %v\n", response)
	if err == nil {
		return &sms.OTPMTResponse{
				Status: result,
			},
			nil
	} else {
		return nil,
			err
	}

}

func (otps *OTPServer) SendTextSMS(ctx context.Context, req *sms.SendSMSRequest) (*sms.SendSMSResponse, error) {
	fmt.Println("GetOTPSMS ...")
	user := message.SendMessageRequest{
		Phone: req.Phone,
		Text:  base64.StdEncoding.EncodeToString([]byte(req.Text)),
	}

	httpReq, err := usecase.GenerateSOAPRequestMTOTP(&user)
	if err != nil {
		fmt.Println("Some problem occurred in request generation")
	}

	response, err := usecase.SoapCallMTOTP(httpReq)
	if err != nil {
		fmt.Println("Problem occurred in making a SOAP call")
	}
	result := fmt.Sprintf("SOAP response: %v\n", response)
	if err == nil {
		return &sms.SendSMSResponse{
				Status: result,
			},
			nil
	} else {
		return nil,
			err
	}

}
