package grpcservice

import (
	"context"
	"log"

	"google.golang.org/grpc"
	message "netacom.vn/netachat/domain"
	sms "netacom.vn/netachat/otp/delivery/grpc/pb"
)

type GrpcSmsService struct {
	clientConn *grpc.ClientConn
}

func (handler *GrpcSmsService) SendTextToUser(phone string, text string) error {

	client := sms.NewOTPServiceClient(handler.clientConn)

	resp, err := client.SendTextSMS(context.Background(), &sms.SendSMSRequest{
		Phone: phone,
		Text:  text,
	})
	if err != nil {
		return err
	}
	log.Printf("otp api response %v\n", resp)
	return nil
}

func NewGrpcClient(cc *grpc.ClientConn) message.SMS_Service {

	return &GrpcSmsService{clientConn: cc}

}
