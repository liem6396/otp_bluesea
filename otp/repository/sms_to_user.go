package smstouser

import (
	"context"
	"log"

	"google.golang.org/grpc"
	sms "netacom.vn/netachat/otp/delivery/grpc/pb"
)

type SMSToUser struct {
	clientConn *grpc.ClientConn
}

func NewGrpcClient(cc *grpc.ClientConn) *SMSToUser {

	return &SMSToUser{clientConn: cc}

}

func (handler *SMSToUser) SendTextToUser(phone string, text string) error {

	client := sms.NewOTPServiceClient(handler.clientConn)

	resp, err := client.SendTextSMS(context.Background(), &sms.SendSMSRequest{
		Phone: phone,
		Text:  text,
	})
	if err != nil {
		return err
	}
	log.Printf("otp api response %v\n", resp)
	return nil
}
