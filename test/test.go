package testsms

import (
	"fmt"
	"log"

	message "netacom.vn/netachat/domain"
)

func SendTextToUser(sms message.SMS_Service) {
	phone := "0939840396"
	OTP := 765123
	text := fmt.Sprintf("Cam on quy khach hang da dang ky thanh cong, tai khoan %v, chi tiet lien he website hoac Hotline: 0901235856", OTP)
	log.Printf("text otp %v\n", text)
	_err := sms.SendTextToUser(phone, text)
	if _err != nil {
		log.Fatalf("call otp api err %v", _err)
	}
}
