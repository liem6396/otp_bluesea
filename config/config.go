package config

import (
	"fmt"

	"github.com/golang/glog"
	"github.com/micro/go-micro/config"
)

func init() {
	fmt.Printf(" config load file \n")
	err := loadAppConfig()
	if err != nil {
		glog.Fatalf("Load app config error: %v", err)
	}
	fmt.Printf(" config load file content %+v :  \n", AppConfig)
}

var AppConfig = &NetaConfig{}

type NetaConfig struct {
	MTSendOTP struct {
		URL      string `yaml:"url"`
		USERNAME string `yaml:"username"`
		PASSWORD string `yaml:"password"`
	} `yaml:"mtsendotp"`
}

func loadAppConfig() error {
	// config.LoadFile(os.Getenv("CONFIG_PATH"))
	config.LoadFile("config/config.yaml")

	config.Scan(AppConfig)

	// glog.Warning(AppConfig.Server.Port)

	return nil
}
