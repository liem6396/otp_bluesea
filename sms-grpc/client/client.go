package main

import (
	"log"

	"google.golang.org/grpc"
	grpcservice "netacom.vn/netachat/otp/microservice"
	testsms "netacom.vn/netachat/test"
)

func main() {

	cc, err := grpc.Dial("167.71.218.111:9996", grpc.WithInsecure())
	if err != nil {
		log.Fatal("err while dial %v", err)
	}
	defer cc.Close()
	smsToUser := grpcservice.NewGrpcClient(cc)
	testsms.SendTextToUser(smsToUser)
}
